#!/bin/sh

SITENAME='My very own site'
SITETITLE='My site :D'

ROOT=$(pwd)
INPUT=$ROOT/pages/
OUTPUT=$ROOT/site/
IMAGES=$ROOT/images/
FONTS=$ROOT/fonts/
TEMPLATES=$ROOT/templates/
BLOGDIR=""

GENERATOR=pandoc

[ -f ./config ] && . ./config

default_config() {
    printf \
'SITENAME="My very own site"
SITETITLE="My site :D"
ROOT=$(pwd)
INPUT=$ROOT/pages/
OUTPUT=$ROOT/site/
IMAGES=$ROOT/images/
FONTS=$ROOT/fonts/
TEMPLATES=$ROOT/templates/
BLOGDIR=/blog

GENERATOR=pandoc
' >config
}

default_styles() {
    printf\
'body {
    background-color: #0f111b;
    color: #FFFFFF;
    margin: 15px auto;
    padding: 0 10px;
}

div#navbar ul {
    list-style: none;
    padding-left: 0
}

div#navbar li {
    display: inline;
    padding: 10 5px;
}

h1,h2,h3,h4,h5,h6 {
	color: #7a5ccc;
	text-decoration: bold;
}

p {
    overflow: hidden;
    max-width: 80ch;
    white-space: wrap;
}

a:link, a:visited {
	color: #5ccc96;
	text-decoration: none;
}

a:hover {
	text-decoration: bold, underline;
	font-size: 110%;
}

img {
    width: 50%;
}
' >styles.css
}

default_templates() {
    printf\

'<!DOCTYPE html>
<html>
    <head>
        <title>%SITETITLE%</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="/styles.css">
    </head>

    <body>
        <div id="headertitle">
            <h1>
                <a href="/index.html">%SITENAME%/</a><a href="./index.html">%CURDIR%</a>
            </h1>
        </div>
' >$TEMPLATES/header.html

    printf\
'    </body>
</html>
' >$TEMPLATES/footer.html
}

new_blogpost() {
    curdate=$(date +%d-%m-%Y)
    printf "Title: "
    read title
    titlenospace=$(echo $title | sed 's/\ //g')
    page=$curdate-$titlenospace
    if [ ! -d $INPUT/$BLOGDIR ]
    then
        mkdir -p $INPUT/$BLOGDIR
        printf "# Blog\n\n" >> $INPUT/$BLOGDIR/index.md
    fi
    printf "# $title\n\n" > $INPUT/$BLOGDIR/$page.md
    printf "1. [$curdate -- $title](./$page.html)\n" >> $INPUT/$BLOGDIR/index.md
}

clean() {
    rm -rf $OUTPUT/*
}

generate() {
    clean
    for page in $(find $INPUT -type f -name *.md)
    do
        local out=$(echo $OUTPUT/${page##$INPUT} | cut -f1 -d '.').html
        mkdir -p $(dirname $out)

        rbasedir=$(dirname $page | cut -c $(echo $INPUT | wc -m)-)
        if [ $(basename $page) = "index.md" -a $(dirname $page) = ${INPUT%/} ]
        then
            rbasedir=""
        fi

        cat $TEMPLATES/header.html |
            sed "s#%SITETITLE%#$SITETITLE#g; s#%SITENAME%#$SITENAME#g;\
            s#%CURDIR%#$rbasedir#g" > $out

        printf '<div id="navbar">\n<ul>\n' >> $out
        echo "<li class=\"sidefile\"><a href=\"../index.html\">../</a></li>" >> $out
        echo "<li class=\"sidefile\"><a href=\"index.html\">index</a></li>" >> $out
        if [ $(basename $(dirname $page)) != $(basename $BLOGDIR) ]
        then
            for other in $(dirname $page)/*
            do
                local bn=$(basename $other)
                local bn_no_ext=$(echo $bn | cut -f1 -d '.')
                local bn_html=$bn_no_ext.html
                if [ $bn_html != "index.html" ]
                then
                    if [ -f $other ]
                    then
                        echo "<li class=\"sidefile\"><a href=\"$bn_html\">$bn_no_ext</a></li>" >> $out
                    elif [ -d $other ]
                    then
                        echo "<li class=\"sidedir\"><a href=\"$bn_no_ext/index.html\">$bn_no_ext</a></li>" >> $out
                    fi
                fi
            done
        fi
        printf '</ul>\n</div>\n' >> $out

        $GENERATOR $page >> $out

        cat $TEMPLATES/footer.html |
            sed "s#%SITETITLE%#$SITETITLE#g; s#%SITENAME%#$SITENAME#g;\
            s#%CURDIR%#$rbasedir#g" >> $out
    done
    cp styles.css $OUTPUT/styles.css

    mkdir -p $OUTPUT/images/
    cp -r $IMAGES $OUTPUT/
    mkdir -p $OUTPUT/fonts/
    cp -r $FONTS $OUTPUT/
}

mkdirs() {
    mkdir -p $ROOT $INPUT $OUTPUT $TEMPLATES
}

init() {
    default_config
    default_styles
    mkdirs
    default_templates
}

help() {
    printf \
'hello :D

Markdown files go in `pages` by default.

The generated html files go in `site` by default.

A "template" header and footer are located in `templates` by default for
specifying the layout of the site.

Any directory created in for markdown files MUST have a containing `index.md`.

Subcommands:
    init       Initialize a new generator site
    gen        Generate the pages
    blogpost   Creates a new blog post and adds it to $BLOGDIR/index.html
    clean      Clean (remove) the output directory
'
}

case $1 in
    init*) init ;;
    gen*) generate ;;
    clean*) clean ;;
    blogpost*) new_blogpost ;;
    *) help ;;
esac
